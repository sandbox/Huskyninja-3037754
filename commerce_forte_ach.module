<?php

define('COMMERCE_FORTE_ACH_TXN_MODE_LIVE', 'live');
define('COMMERCE_FORTE_ACH_TXN_MODE_DEVELOPER', 'sandbox');

/**
 * Implements hook_help().
 */
function commerce_forte_ach_help($path, $arg) {
  switch ($path) {
    case 'admin/help/commerce_forte_ach':
      $output = '<h3>' . t('INTRODUCTION') . '</h3>';
      $output .= '<p>' . t('1. This module allows Drupal Commerce customers to pay using the Forte payment gateway with the a custom eCheck. The eCheck code for Commerce Payment can be found at <a href="https://www.drupal.org/project/commerce/issues/1661548">https://www.drupal.org/project/commerce/issues/1661548</a>') . '</p>';
	  $output .= '<p>' . t('2. This module is a basic hack through of <a href="https://www.drupal.org/project/commerce_forte">Commerce Forte</a> 7.x-1.2, and uses the Commerce Payment eCheck (#1 above) for the payment method.') .  '</p>';

      $output .= '<h3>' . t('CONFIGURATION') . '</h3>';
      $output .= '<p>' . t('1. Enable the Forte ACH Payment under Administration -> Store > Configuration -> Payment Methods') . '</p>';
      $output .= '<p>' . t('2. Enter your Forte API details under Administration -> Store -> Configuration -> Payment Methods -> Forte ACH Payments -> edit Operations.') . '</p>';
	  $output .= '<p>' . t('3. Call Forte support to help you figure out the Location ID and Accoutn ID.') . '</p>';
      $output .= '<h3>' . t('NOTE') . '</h3>';
      $output .= '<p>' . t('You will need your Forte Merchant Account ID and Rest Account ID. You will also need the API Login ID and Transaction Key for to use encryption that was sent to you when your Rest Forte account was set up or contact Forte Support team for the Rest API credentials.') . '</p>';

      return $output;
  }
}

/**
 * Implements hook_commerce_payment_method_info().
 */
function commerce_forte_ach_commerce_payment_method_info() {
	$payment_methods = array();
  
	$payment_methods['forte_ach'] = array(
		'base' => 'commerce_forte_ach',
		'title' => t('Forte ACH Payments'),
		'short_title' => t('Forte ACH'),
		'display_title' => t('Pay with eCheck using Forte'),
		'description' => t('Uses Forte to handle ACH / eCheck transactions.'),
	);

  return $payment_methods;
}

/**
 * Returns the default settings for the Forte.Net payment method.
 */
function commerce_forte_ach_default_settings() {
	return array(
		'login' => '',
		'tran_key' => '',
		'location_id' => '',
		'account_id' => '',
		'txn_mode' => COMMERCE_FORTE_ACH_TXN_MODE_LIVE,
		'txn_type' => COMMERCE_CREDIT_AUTH_CAPTURE,
		'cardonfile' => FALSE,
		'continuous' => FALSE,
		'email_customer' => FALSE,
		'log' => array('request' => '0', 'response' => '0'),
	);
}

/**
 * Payment method callback: settings form.
 */
function commerce_forte_ach_settings_form($settings = NULL) {
	module_load_include('inc', 'commerce_payment', 'includes/commerce_payment.echeck');
	
	// Merge default settings into the stored settings array.
	$settings = (array) $settings + commerce_forte_ach_default_settings();

	$form['login'] = array(
		'#type' => 'textfield',
		'#title' => t('Rest API Access ID'),
		'#description' => t('Your REST API ACCESS ID is different from the username you use to login to your REST Forte.Net account. Once you login, browse to your API tab and click the <em>Rest API Access ID and Rest API Secure Key</em> link to find your REST API Access ID. If you are using a new REST forte.Net account, you may still need to generate an ID.'),
		'#default_value' => $settings['login'],
		'#required' => TRUE,
	);
	$form['tran_key'] = array(
		'#type' => 'textfield',
		'#title' => t('Rest API Secure Key'),
		'#description' => t('Your Rest API Secure Key can be found on the same screen as your Rest API Access ID. However, it will not be readily displayed. You must generate the secure key a form to see your REST API Secure Key.'),
		'#default_value' => $settings['tran_key'],
		'#required' => TRUE,
	);
	$form['location_id'] = array(
		'#type' => 'textfield',
		'#title' => t('Location ID'),
		'#description' => t('Old Merchant ID. Forte calls this the Location ID, since it is tied to a location under the main customer. I don\'t know - Forte is confusing.'),
		'#default_value' => $settings['location_id'],
		'#required' => TRUE,
	);
	$form['account_id'] = array(
		'#type' => 'textfield',
		'#title' => t('Account ID'),
		'#description' => t('Old Rest Account ID. Forte called this the Account ID, which is the ID of the root account owner, or customer. Your can find it on some screens in Forte near the name of customer (or merchant). Who knows. Call support and ask.'),
		'#default_value' => $settings['account_id'],
		'#required' => TRUE,
	);
	$form['txn_mode'] = array(
		'#type' => 'radios',
		'#title' => t('Transaction mode'),
		'#description' => t('Adjust to live transactions when you are ready to start processing real payments.') . '<br />' . t('Only specify a developer test account if you login to your account through https://sandbox.paymentsgateway.net.'),
		'#options' => array(
			COMMERCE_FORTE_ACH_TXN_MODE_LIVE => t('Live transactions in a live account'),
			COMMERCE_FORTE_ACH_TXN_MODE_DEVELOPER => t('Developer test account transactions'),
		),
		'#default_value' => $settings['txn_mode'],
	);
	$form['txn_type'] = array(
		'#type' => 'radios',
		'#title' => t('Default transaction type'),
		'#description' => t('The default will be used to process transactions during checkout. This thing don\'t do credit cards, so I have no idea if this works or not.'),
		'#options' => array(
			COMMERCE_CREDIT_AUTH_CAPTURE => t('Authorization and capture'),
			COMMERCE_CREDIT_AUTH_ONLY => t('Authorization only (requires manual or automated capture after checkout)'),
		),
		'#default_value' => $settings['txn_type'],
	);
	$form['email_customer'] = array(
		'#type' => 'checkbox',
		'#title' => t('Tell forte.net to e-mail the customer a receipt based on your account settings. Not sure this works, as I cannot find a way to send this setting to Forte.'),
		'#default_value' => $settings['email_customer'],
	);
	$form['log'] = array(
		'#type' => 'checkboxes',
		'#title' => t('Log the following messages for debugging'),
		'#options' => array(
			'request' => t('API request messages'),
			'response' => t('API response messages'),
		),
		'#default_value' => $settings['log'],
	);

	return $form;

}

/**
 * Payment method callback: checkout form.
 */
function commerce_forte_ach_submit_form($payment_method, $pane_values, $checkout_pane, $order) {
	module_load_include('inc', 'commerce_payment', 'includes/commerce_payment.echeck');
	
	// Prepare the fields to include on the form.
	$fields = array(
		'owner' => TRUE,
		'code' => '',
	);
	
	$fields['acct_name'] = 'acct_name';
	return commerce_payment_echeck_form($fields);
}

/**
 * Payment method callback: checkout form validation.
 */
function commerce_forte_ach_submit_form_validate($payment_method, $pane_form, $pane_values, $order, $form_parents = array()) {

	module_load_include('inc', 'commerce_payment', 'includes/commerce_payment.echeck');
	
	$settings = array(
		'form_parents' => array_merge($form_parents, array('echeck')),
	);

	if (!commerce_payment_echeck_validate($pane_values['echeck'], $settings)) {
		return FALSE;
	}
}

/**
 * Payment method callback: checkout form submission.
 */
function commerce_forte_ach_submit_form_submit($payment_method, $pane_form, $pane_values, $order, $charge) {
	
	module_load_include('inc', 'commerce_payment', 'includes/commerce_payment.echeck');

	$prevent_transaction = FALSE;
	
	if ($charge['amount'] == 0) {
		$prevent_transaction = TRUE;
	}
	
    if ($prevent_transaction) {
		// Create a transaction to log the skipped transaction and display a
		// helpful message to the customer.
		$transaction = commerce_payment_transaction_new('forte_ach', $order->order_id);
		$transaction->amount = $charge['amount'];
		$transaction->currency_code = $charge['currency_code'];
		$transaction->status = COMMERCE_PAYMENT_STATUS_FAILURE;
		$transaction->message = t('Invalid @amount transaction not attempted.', array('@amount' => commerce_currency_format($charge['amount'], $charge['currency_code'])));
		commerce_payment_transaction_save($transaction);

		drupal_set_message(t('We encountered an error processing your transaction. Please contact us to resolve the issue.'), 'error');
		return FALSE;
    }
	
	$order_wrapper = entity_metadata_wrapper('commerce_order', $order);
	
	// Get the default transaction type from the payment method settings.
	$txn_type = $payment_method['settings']['txn_type'];
	
	// If txn_type has been specified in the pane values array, such as through
	// the special select element we alter onto the payment terminal form, use
	// that instead.
	if (!empty($pane_values['txn_type'])) {
		$txn_type = $pane_values['txn_type'];
	}
	
	$nvp = array(
		'action' => commerce_forte_ach_txn_type($txn_type),
		'authorization_amount' => number_format(commerce_currency_amount_to_decimal($charge['amount'], $charge['currency_code']), 2, '.', ''),
		'echeck' => array(
			'sec_code' => 'WEB',
			'account_type' => 'Checking',
			'routing_number' => $pane_values['echeck']['aba_code'],
			'account_number' => $pane_values['echeck']['acct_num'],
			'account_holder' => $pane_values['echeck']['acct_name'],
		),
	);
	
	// Add additional transaction invormation to the request array.
	$nvp += array(
		// Order Information.
		'order_number' => $order->order_number,
		// Customer Information.
		// use if capturing customers as users
		'customer_id' => substr($order->uid, 0, 20),
		// use if customer is anon
		// 'customer_id' => $order->order_number,
	);

	// Prepare the billing address for use in the request.
	if (isset($order->commerce_customer_billing) && $order_wrapper->commerce_customer_billing->value()) {
		$billing_address = $order_wrapper->commerce_customer_billing->commerce_customer_address->value();

		if (empty($billing_address['first_name'])) {
			$name_parts = explode(' ', $billing_address['name_line']);
			$billing_address['first_name'] = array_shift($name_parts);
			$billing_address['last_name'] = implode(' ', $name_parts);
		}
	}

	// Prepare the shipping address for use in the request.
	if (isset($order->commerce_customer_shipping) && $order_wrapper->commerce_customer_shipping->value()) {
		$shipping_address = $order_wrapper->commerce_customer_shipping->commerce_customer_address->value();

		if (empty($shipping_address['first_name'])) {
			$name_parts = explode(' ', $shipping_address['name_line']);
			$shipping_address['first_name'] = array_shift($name_parts);
		$shipping_address['last_name'] = implode(' ', $name_parts);
		}
	}

	//Adding condition of variable Billing Address.
	$bill_firstname = empty($billing_address['first_name']) ? "N/A" : $billing_address['first_name'];
	$bill_lastname = empty($billing_address['last_name']) ? "N/A" : $billing_address['last_name'];
	$bill_companyname = empty($billing_address['organisation_name']) ? "N/A" : $billing_address['organisation_name'];
	$bill_thoroughfare = empty($billing_address['thoroughfare']) ? "N/A" : $billing_address['thoroughfare'];
	$bill_locality = empty($billing_address['locality']) ? "N/A" : $billing_address['locality'];
	$bill_admin_area = empty($billing_address['administrative_area']) ? "N/A" : $billing_address['administrative_area'];
	$bill_postal_code = empty($billing_address['postal_code']) ? "N/A" : $billing_address['postal_code'];

	//Adding condition of variable Shipping Address.
	$ship_firstname = empty($shipping_address['first_name']) ? "N/A" : $shipping_address['first_name'];
	$ship_lastname = empty($shipping_address['last_name']) ? "N/A" : $shipping_address['last_name'];
	$ship_companyname = empty($shipping_address['organisation_name']) ? "N/A" : $shipping_address['organisation_name'];
	$ship_thoroughfare = empty($shipping_address['thoroughfare']) ? "N/A" : $shipping_address['thoroughfare'];
	$ship_locality = empty($shipping_address['locality']) ? "N/A" : $shipping_address['locality'];
	$ship_admin_area = empty($shipping_address['administrative_area']) ? "N/A" : $shipping_address['administrative_area'];
	$ship_postal_code = empty($shipping_address['postal_code']) ? "N/A" : $shipping_address['postal_code'];	
	
	$nvp += array(
		'billing_address' => array(
			'first_name' => substr($bill_firstname, 0, 50),
			'last_name' => substr($bill_lastname, 0, 50),
			'company_name' => substr($bill_companyname, 0, 50),
			'email' => substr($order->mail, 0, 255),
			'physical_address' => array(
				'street_line1' => substr($bill_thoroughfare, 0, 60),
				'locality' => substr($bill_locality, 0, 40),
				'region' => substr($bill_admin_area, 0, 40),
				'postal_code' => substr($bill_postal_code, 0, 20),
			),
		),
		'shipping_address' => array(
			'first_name' => substr($ship_firstname, 0, 50),
			'last_name' => substr($ship_lastname, 0, 50),
			'company_name' => substr($ship_companyname, 0, 50),
			'physical_address' => array(
				'street_line1' => substr($ship_thoroughfare, 0, 60),
				'locality' => substr($ship_locality, 0, 40),
				'region' => substr($ship_admin_area, 0, 40),
				'postal_code' => substr($ship_postal_code, 0, 20),
			),
		),
	);
	
	// Submit the request to forte.
	$response = commerce_forte_ach_request($payment_method, $nvp);
	
	// Prepare a transaction object to log the API response.
	$transaction = commerce_payment_transaction_new('forte_ach', $order->order_id);
	$transaction->instance_id = $payment_method['instance_id'];
	$transaction->amount = $charge['amount'];
	$transaction->currency_code = $charge['currency_code'];
	
	$transaction->payload[REQUEST_TIME] = $response;	
	
	// If we didn't get an approval response code...
	if ($response['response']['response_code'] != 'A01') {
		// Create a failed transaction with the error message.
		$transaction->status = COMMERCE_PAYMENT_STATUS_FAILURE;
	}
	else {

		// Set the transaction status based on the type of transaction this was.
		switch ($txn_type) {
			case COMMERCE_CREDIT_AUTH_ONLY:
				$transaction->status = COMMERCE_PAYMENT_STATUS_PENDING;
				break;

			case COMMERCE_CREDIT_AUTH_CAPTURE:
				$transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
				break;
			default:
				$transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
		}

	}
		
	// Build a meaningful response message.
	$message = array(
		'<b>' . commerce_forte_ach_reverse_txn_type(commerce_forte_ach_txn_type($payment_method['settings']['txn_type'])) . '</b>',
		'<b>' . t('Authorized Date & Time: @date', array('@date' => format_date(REQUEST_TIME, 'short'))) . '</b>',
		'<b>' . t('Response code: ') . '</b>' . check_plain($response['response']['response_code']),
		'<b>' . ($response['response']['response_code'] != 'A01' ? t('REJECTED') : t('ACCEPTED')) . ':</b> ' . check_plain($response['response']['response_desc']),
	);
	
	$transaction->message = implode('<br />', $message);
	
	// Save the transaction information.
	commerce_payment_transaction_save($transaction);
	
	
	// If the payment failed, display an error and rebuild the form.
	if ($response['response']['response_code'] != 'A01') {
		drupal_set_message(t('There was an error processing your request. Please try again.'), 'error');
		return FALSE;
	}
	
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function commerce_forte_ach_form_commerce_payment_order_transaction_add_form_alter(&$form, &$form_state) {
	return TRUE;
}

/**
 * Set the param request to forte server.
 */
function commerce_forte_ach_request($payment_method, $nvp = array()) {
	
	$merchant_id = 'loc_' . $payment_method['settings']['location_id'];
	$account_id = 'act_' . $payment_method['settings']['account_id'];
	$act_loc_url = 'accounts/' . $account_id . '/locations/' . $merchant_id . '/transactions/';
	$service_url = commerce_forte_ach_server_url($payment_method['settings']['txn_mode']) . $act_loc_url;
	
	// Allow modules to alter parameters of the API request.
	drupal_alter('commerce_forte_ach_request', $nvp, $payment_method);
	
	if ($payment_method['settings']['log']['request'] == 'request') {
		
		$log_nvp = $nvp;
		
		watchdog('Commerce Forte ACH', 'Forte.Net AIM request to @url: !param', array(
			'@url' => $service_url,
			'!param' => '<pre>' . check_plain(print_r($log_nvp, TRUE)) . '</pre>',
			'watchdog_debug' => WATCHDOG_DEBUG,
			)
		);
		
	}
	
	// Encode the API credentials for the backend settings.
	$apiloginid = $payment_method['settings']['login'];
	$securetransactionkey = $payment_method['settings']['tran_key'];
	$rest_hash = $apiloginid . ':' . $securetransactionkey;
	$auth_token = base64_encode($rest_hash);
	$curlopt_customrequests = "POST";
	
	// Get the forte server response using the curl.
	// here's where the rubber meets the road
	$response = commerce_forte_ach_curl_request($service_url, $auth_token, $curlopt_customrequests, $payment_method, $nvp);
	
	// Log the response if specified.
	if ($payment_method['settings']['log']['response'] == 'response') {
		watchdog('Commerce Forte ACH', 'Forte.Net ACH response: !param', array(
			'!param' => '<pre>' . check_plain(print_r($response, TRUE)) . '</pre>',
			'watchdog_debug' => WATCHDOG_DEBUG,
			)
		);
	}
	
	return $response;
	
}

/**
 * Returns the curl response.
 */
function commerce_forte_ach_curl_request($service_url, $auth_token, $curlopt_customrequests, $payment_method, $nvp = array()) {
	$curl = curl_init();
	
	curl_setopt_array($curl, array(
		CURLOPT_URL => $service_url,
		CURLOPT_RETURNTRANSFER => TRUE,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => $curlopt_customrequests,
		CURLOPT_HTTPHEADER => array(
			"accept: application/json",
			"authorization: Basic " . $auth_token,
			"cache-control: no-cache",
			"content-type: application/json",
			"postman-token: da3ddbe1-9987-b414-288e-f8fa7352fb1a",
			"x-forte-auth-account-id: act_" . $payment_method['settings']['account_id'],
		),
	));
	$data = drupal_json_encode($nvp);

	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, TRUE);
	curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
	$curl_response = curl_exec($curl);
	$err = curl_error($curl);

	curl_close($curl);

	if ($err) {
		watchdog('Commerce Forte ACH', 'CURL ERROR #: !param', array(
			'!param' => '<pre>' . check_plain(print_r($err, TRUE)) . '</pre>',
			'watchdog_debug' => WATCHDOG_DEBUG,
		)
		);
	}
	else {
		watchdog('Commerce Forte ACH', 'CURL Response #: !param', array(
			'!param' => '<pre>' . check_plain(print_r($curl_response, TRUE)) . '</pre>',
			'watchdog_debug' => WATCHDOG_DEBUG,
		)
		);
	}

	// Make the response an array and trim off the encapsulating characters.
	$response = drupal_json_decode($curl_response);
	return $response;
}

/**
 * Returns the message text for an AVS response code.
 */
function commerce_forte_ach_response($code) {
	switch ($code) {
		case 'A':
			return t('Address (Street) matches, ZIP does not');
		case 'B':
			return t('Address information not provided for AVS check');
		case 'E':
			return t('AVS error');
		case 'G':
			return t('Non-U.S. Card Issuing Bank');
		case 'N':
			return t('No Match on Address (Street) or ZIP');
		case 'P':
			return t('AVS not applicable for this transaction');
		case 'R':
			return t('Retry – System unavailable or timed out');
		case 'S':
			return t('Service not supported by issuer');
		case 'U':
			return t('Address information is unavailable');
		case 'W':
			return t('Nine digit ZIP matches, Address (Street) does not');
		case 'X':
			return t('Address (Street) and nine digit ZIP match');
		case 'Y':
			return t('Address (Street) and five digit ZIP match');
		case 'Z':
			return t('Five digit ZIP matches, Address (Street) does not');
		default:
			return t('AVS error unlisted.');
	}
	return t('Unknown or undefined error code.');
}

/**
 * Returns the URL to the Forte.Net server determined by transaction mode.
 *
 * @param string $txn_mode
 *   The transaction mode that relates to the live or test server.
 *
 * @return string
 *   The URL to use to submit requests to the Forte.Net server.
 */
function commerce_forte_ach_server_url($txn_mode) {
	switch ($txn_mode) {
		case COMMERCE_FORTE_ACH_TXN_MODE_LIVE:
			return variable_get('commerce_forte_ach_server_url_live', 'https://api.forte.net/v2/');

		case COMMERCE_FORTE_ACH_TXN_MODE_DEVELOPER:
			return variable_get('commerce_forte_ach_server_url_dev', 'https://sandbox.forte.net/api/v2/');
	}
}

/**
 * Returns the transaction type string for Forte.Net that corresponds to the.
 *
 * Drupal Commerce constant.
 *
 * @param string $txn_type
 *   A Drupal Commerce transaction type constant.
 */
function commerce_forte_ach_txn_type($txn_type) {
	switch ($txn_type) {
		case COMMERCE_CREDIT_AUTH_ONLY:
			return 'authorize';
		case COMMERCE_CREDIT_AUTH_CAPTURE:
			return 'sale';
		case COMMERCE_CREDIT_CAPTURE_ONLY:
			return 'CAPTURE_ONLY';
  }
}

/**
 * Returns the description of an Forte.Net transaction type.
 *
 * @param string $txn_type
 *   An Forte.Net transaction type string.
 */
function commerce_forte_ach_reverse_txn_type($txn_type) {
	switch ($txn_type) {
		case 'authorize':
			return t('Authorization only');
		case 'sale':
			return t('Authorization and capture');
		case 'PRIOR_AUTH_CAPTURE':
			return t('Prior authorization capture');
		case 'CAPTURE_ONLY':
			return t('Capture only');
		case 'CREDIT':
			return t('Credit');
		case 'VOID':
			return t('Void');
  }
}